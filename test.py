# -*- coding: utf-8 -*-
"""
Created on Sun Nov  5 14:26:51 2017

@author: Yunyun
"""
import pyFAI
import fabio
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import numpy as np
import glob
import argparse
import multiprocessing
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation 
import copy


#%%
#Naming
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1/analysis/*_nrm.edf"
nrm_files = sorted(glob.glob(file_folder))[223-104: 607-104]

#sample_nrm = [fabio.open(s) for s in nrm_files]
sample_nrm = []
for s in nrm_files:
    sample_nrm.append(fabio.open(s).data)
sample_nrm = np.array(sample_nrm)
"""
"""
    #sums
tb_sum = []
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    tb_sum.append((tb[0] * tb[1]).sum())
    
tb_sum = np.array(tb_sum)
"""
#%%

#coordinates
dev1_x_rel = np.array([ 47.   ,  45.15 ,  40.15 ,  35.15 ,  30.15 ,  25.15 ,  20.15 ,
        15.15 ,  10.15 ,   9.15 ,   8.15 ,   7.15 ,   6.15 ,   5.15 ,
         4.15 ,   3.15 ,   2.15 ,   1.15 ,   0.15 ,   0.075,  -0.   ,
        -0.075,  -0.15 ,  -0.3  ])

dev2_x_rel=np.array([ 46.5  ,  45.   ,  40.   ,  35.   ,  30.   ,  25.   ,  20.   ,
        15.   ,  10.   ,   9.   ,   8.   ,   7.   ,   6.   ,   5.   ,
         4.   ,   3.   ,   2.   ,   1.   ,   0.5  ,  -0.077,  -0.152,
        -0.227,  -0.5  ])
#%%water background for dev1 0.1s
"""
back_ground
for 0.1s

return bg_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1/dat/*_0001.dat"
#air 0.1s
#nrm_files = sorted(glob.glob(file_folder))[223-104: 607-104]
#x_range = np.arange(222,606)
#water 0.1s
nrm_files = sorted(glob.glob(file_folder))[991-104: 1375-104]
x_range = np.arange(991, 1375)
    

#matrix
bg_01s_matrix =[]
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    bg_01s_matrix.append(tb[1].values[:1318])

bg_01s_matrix = np.array(bg_01s_matrix)
q_range = tb[0].values
#%%water background for dev1 1s
"""
back ground
for 1s
return bg_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[1375-104: 1759-104]
x_range = np.arange(1375, 1758)

#matrix
bg_1s_matrix =[]
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    if int(f.split('saxs_')[1][0:5])%16 != 1:
        bg_1s_matrix.append(tb[1].values[:1318])

bg_1s_matrix = np.array(bg_1s_matrix)
q_range = tb[0].values
#%%water background for dev2 1s
"""
background 
for 1s dev2
retrun bg2_1s_matrix

_start_idx
"""
_start_idx = 368
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1: _start_idx-1+368]

#matrix
bg2b_1s_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
    #first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    #last_ind = np.where(q_range == tb.loc[tb[1].size-1, 0])[-1]
    #bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(last_ind)] = tb[1].values

bg2b_1s_matrix = np.array(bg2b_1s_matrix)
q_range = tb[0].values[:1318]

#%%air background dev2s2a 1s
"""
background air unused coordinates
for 1s dev2s2a
retrun bg2s2a_1s_matrix

_start_idx
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2a/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1: _start_idx-1+368]

#matrix
bg2s2a_1s_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        bg2s2a_1s_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
    #first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    #last_ind = np.where(q_range == tb.loc[tb[1].size-1, 0])[-1]
    #bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(last_ind)] = tb[1].values

bg2s2a_1s_matrix = np.array(bg2s2a_1s_matrix)
q_range = tb[0].values[:1318]

#%%water BACKGROUND dev2s2b 1s
"""
background water unused coordinates
for 1s dev2s2b
retrun bg2s2b_1s_matrix

_start_idx
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1: _start_idx-1+368]

#matrix
bg2s2b_1s_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        bg2s2b_1s_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
    #first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    #last_ind = np.where(q_range == tb.loc[tb[1].size-1, 0])[-1]
    #bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(last_ind)] = tb[1].values

bg2s2b_1s_matrix = np.array(bg2s2b_1s_matrix)
q_range = tb[0].values[:1318]

#%%non-reactive flow backgroud dev2s2c 1s
"""
background water unused coordinates
for 1s dev2s2b
retrun bg2s2c_1s_matrix

_start_idx
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2c/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1: _start_idx-1+368]

#matrix
bg2s2c_1s_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        bg2s2c_1s_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
    #first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    #last_ind = np.where(q_range == tb.loc[tb[1].size-1, 0])[-1]
    #bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(last_ind)] = tb[1].values

bg2s2c_1s_matrix = np.array(bg2s2c_1s_matrix)
q_range = tb[0].values[:1318]

#%%
"""
background 
for 1s dev2 post sample
retrun bg2_pe_1s_matrix

_start_idx

_start_idx = 1 #first no of image
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2bgpe/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1: _start_idx-1+368]

#matrix
bg2b_1s_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
    #first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    #last_ind = np.where(q_range == tb.loc[tb[1].size-1, 0])[-1]
    #bg2b_1s_matrix[idx-idx/16-1][int(first_ind):int(last_ind)] = tb[1].values

bg2b_1s_matrix = np.array(bg2b_1s_matrix)
q_range = tb[0].values[:1318]
"""
# =============================================================================
# above background

#below samples
# =============================================================================
#%%
"""
dev1s1a 1s flow_rate=
retrun data_matrix

file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1s1a/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[0:0+384]

#matrix
data_matrix = []
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    if int(f.split('saxs_')[1][0:5])%16 != 1:
        data_matrix.append(tb[1].values[:1318])

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape
"""
#%%
"""
dev1s1a 1s flow_rate=
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1s1a/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[0:0+384] #change the first number and the second one before the +

#matrix
data_matrix = []
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    if int(f.split('saxs_')[1][0:5])%16 != 1: 
        data_matrix.append(tb[1].values[:1318])

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape

#%%
"""
dev1s1b 1s flow_rate=
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1s1b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[0:0+384] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((384, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5])
    #if idx%16 != 1: 
    first_ind = np.where(q_range == tb.loc[0, 0])[-1]
    data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape

#%%
"""
dev2b 1s flow_rate= narrow-flow1 5:1:5 300ul/h
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[736:736+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - 737
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape
nff_matrix = copy.deepcopy(data_matrix)
#%%
"""
dev2b 1s flow_rate= narrow-flow2 5:1:5 300ul/h
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[1104:1104+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - 1105
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape
nff_matrix = copy.deepcopy(data_matrix)

#%%
"""
dev2b 1s flow_rate= mid-narrow-flow1 2.5:1:2.5 
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[1472:1472+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - 1473
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape

#%%
"""
dev2b 1s flow_rate= mid-narrow-flow1 2.5:1:2.5 
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[1840:1840+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - 1841
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape

#%%
"""
dev2b 1s flow_rate= focusing-flow1 1:1:1 
retrun data_matrix
"""
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[2208:2208+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - 2209
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print data_matrix.shape
#fcf_matrix = copy.deepcopy(data_matrix)
# =============================================================================
# 04/11/2017 21:40:00 before
# =============================================================================
#%%
"""
dev2c 1s flow_rate= focusing-flow 1:1:1 sum 100 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1 
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape
#%%
"""
dev2c 1s flow_rate= focusing-flow 2.5:1:2.5 sum 100 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 369 
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%%
"""
dev2c 1s flow_rate= focusing-flow 5:1:5 sum 100 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 737
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% THIS DOES NOT WORK
"""
dev2d 1s flow_rate= focusing-flow 5:1:5 sum 300 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1 
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape
#%%
"""
dev2d 1s flow_rate= focusing-flow 2.5:1:2.5 sum 300 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 737
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]

data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% THIS DOES NOT WORK
"""
dev2d 1s flow_rate= focusing-flow 1:1:1 sum 300 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1105
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% 
"""
dev2h2 1s flow_rate= focusing-flow 5:1:5 sum 300 ul/h
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% 
"""
dev2s2a 1s unused spot new air backgroud
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% 
"""
dev2s2b 1s unused spot new water backgroud
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2b/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape

#%% 
"""
dev2s2c 1s flow sum 300 ratio 1to1to1 no-initialter of reaction we did yesterday
retrun data_matrix

change file_folder to corresponding directory
change only _start_idx, this is the number of the first image
"""
_start_idx = 1
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2c/dat/*_0001.dat"
nrm_files = sorted(glob.glob(file_folder))[_start_idx-1:_start_idx-1+368] #change the first number and the second one before the +

#matrix
data_matrix = np.zeros((345, 1318),dtype=float)
for f in nrm_files:
    tb = pd.read_table(f, sep='\s+', skiprows=1, header=None)
    idx = int(f.split('saxs_')[1][0:5]) - _start_idx
    if idx%16 != 1: 
        first_ind = np.where(q_range == tb.loc[0, 0])[-1]
        if int(first_ind)+tb[1].size > 1318:
            tb = tb.drop(tb.index[-1])
        data_matrix[idx-idx/16-1][int(first_ind):int(first_ind)+tb[1].size] = tb[1].values[:1318]
        
data_matrix = np.array(data_matrix)
q_range = tb[0].values[:1318]
print "(345,1318) should be the right dimension. Current dimension: (%i, %i)" % data_matrix.shape


#%%SUBTRACTION
"""
substraction
data_matrix - bg_matrix
retrun subs_matrix
"""
#subs_matrix = data_matrix - bg_1s_matrix
#subs_matrix1 = nff_matrix - bg2b_1s_matrix
#subs_matrix2 = fcf_matrix - bg2b_1s_matrix
#subs_matrix = data_matrix - bg2b_1s_matrix # if device gets changed change the naming of bg2b_1s_matrix
#subs_matrix = data_matrix - bg2s2a_1s_matrix
subs_matrix = data_matrix - bg2s2b_1s_matrix

#%%PLOT1
"""
plot channel position (vertical axis waterfall)
change _position_ac. this is the frame number of position across the channel
"""
_position_ac = 2 # 0:end of channel, 22: beginning of channel
######################################
fig, ax = plt.subplots(1, 1, figsize=[10,8],subplot_kw={'projection':'3d'})
#subs_matrix = subs_matrix.reshape(15,24,1318)
subs_matrix = subs_matrix.reshape(15,23,1318)
cNorm = colors.Normalize(vmin=0, vmax=14)
scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
#for ind in range(subs_matrix.shape[0]):
q_wf = np.inner(np.ones(subs_matrix.shape[0])[:,None],np.log10(q_range)[:,None])
i_wf = np.log10(subs_matrix[:,_position_ac,:]) 
v_frame = np.linspace(0,15,15)
#ax.set_xlim([-2.0,-1.0])
#ax.set_zlim([-2,8])
ax.set_yticks(np.arange(0,16))
ax.set_title(_position_ac)
ax.plot_wireframe(q_wf, v_frame[:,None], i_wf, cstride=0, colors=cm.jet((v_frame-v_frame.min())/(v_frame.max()-v_frame.min())))
#%%PLOT2
"""
plot flow_direction (horizontal axis waterfall)
change _position_al. this is the frame number of each position along the channel
"""
_position_al = 9 # 0:top wall, 14: bottom wall
fig, ax = plt.subplots(1, 1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(15,23,1318)
cNorm = colors.Normalize(vmin=0, vmax=14)
scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
x_rel = dev2_x_rel
#for ind in range(subs_matrix.sh6ape[0]):
q_wf = np.inner(np.ones(subs_matrix.shape[1])[:,None],np.log10(q_range)[:,None])
i_wf = subs_matrix[_position_al,:,:]
#ax.set_xlim([-2.0,-1.0])
ax.set_zlim([-2000,2000])
ax.set_title(_position_al)
ax.plot_wireframe(q_wf, x_rel[:,None], i_wf, cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))
#%%PLOT3
"""
plot flow_direction_diff (horizontal axis waterfall)
change _position_al. this is the frame number of each position along the channel
"""
_position_al = 7 # 0:top wall, 14: bottom wall
fig, ax = plt.subplots(1, 1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(15,23,1318)
cNorm = colors.Normalize(vmin=0, vmax=14)
scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
x_rel = dev2_x_rel
#for ind in range(subs_matrix.shape[0]):
q_wf = np.inner(np.ones(subs_matrix.shape[1])[:,None],np.log10(q_range)[:,None])
i_wf = subs_matrix[_position_al,:,:] - subs_matrix[_position_al,22,:] # change the number beofore ,:,: both
#ax.set_xlim([-2.0,-1.0])
ax.set_zlim([-2000,5000])
ax.set_title(_position_al)
ax.plot_wireframe(q_wf, x_rel[:,None], i_wf, cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))
#%%
#%%
"""
animation single
"""
fig, ax = plt.subplots(1,1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(16,23,1318)
ax.view_init(45,45)
ax.set_zlim([-6,8])
i_wf = np.log10(subs_matrix.transpose((1,0,2)))[:,1:15,:]
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range)[:,None])
v_frame = np.linspace(0,14,14)
wframe = None
while True:
    for idx in range(0,23):
        if wframe:
            ax.collections.remove(wframe)
        wframe = ax.plot_wireframe(q_wf, v_frame[:,None], i_wf[22-idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        ax.set_title(idx)
        plt.pause(0.1)
        
#%%
"""
animation compare
"""
fig, (ax1,ax2) = plt.subplots(2,1, figsize=[10,14],subplot_kw={'projection':'3d'})
subs_matrix1 = subs_matrix1.reshape(15,23,1318)
subs_matrix2 = subs_matrix2.reshape(15,23,1318)
ax1.view_init(45,45)
ax1.set_zlim([-4,6])
ax2.view_init(45,45)
ax2.set_zlim([-4,6])
i_wf1 = np.log10(subs_matrix1.transpose((1,0,2)))
i_wf2 = np.log10(subs_matrix2.transpose((1,0,2)))
q_wf = np.inner(np.ones(i_wf1.shape[1])[:,None],np.log10(q_range)[:,None])
v_frame = np.linspace(0,15,15)
wframe1 = None
wframe2 = None
fig.subplots_adjust(hspace=0)
ax1.set_title('5:1:5')
ax2.set_title('1:1:1')
while True:
    for idx in range(0,23):
        fig.suptitle(idx)
        if wframe1:
            ax1.collections.remove(wframe1)
            ax2.collections.remove(wframe2)
        wframe1 = ax1.plot_wireframe(q_wf, v_frame[:,None], i_wf1[22-idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        wframe2 = ax2.plot_wireframe(q_wf, v_frame[:,None], i_wf2[22-idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        plt.pause(0.8)
        
#%%
"""
fig, ax = plt.subplots(1,1,figsize=[24,10])
#ax.scatter(x_range, tb_sum)
ax.set_ylim([0,10000])
tb_sum = tb_sum.reshape(16,24).transpose()
cNorm = colors.Normalize(vmin=0, vmax=15)
scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
for i in xrange(0,24):
    ax.scatter(np.arange(991+i,1375,24), tb_sum[i], c=scalarmap.to_rgba(i))
 """