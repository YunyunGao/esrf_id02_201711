#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 15:24:00 2017

@author: yunyun
"""

import pyFAI
import fabio
import numpy as np
import glob
import pandas as pd
import scipy.misc
import os

detector = pyFAI.detectors.Detector(pixel1=88.3464e-6, pixel2=88.3464e-6, max_shape=(1920, 1920))
ai = pyFAI.AzimuthalIntegrator(dist=9.98818, poni1=988.2*88.3464e-6, 
                               poni2=906.9*88.3464e-6, 
                               detector=detector, wavelength=0.95057e-10)
mask = fabio.open("/home/yunyun/Works/mo/mask_10m.edf").data
mask[np.where(mask == 246)] = 1
writer = pyFAI.io.DefaultAiWriter(filename=None, engine=ai)


def integration(sample_folder, s_idx1, s_idx2, bg_folder, bg_idx1, bg_idx2, mask, dat_folder, norm_method='trans'):
    sample_files = sorted(glob.glob(os.path.join(sample_folder, '*_raw.edf')))[s_idx1-1: s_idx2]
    bg_files = sorted(glob.glob(os.path.join(bg_folder, '*_raw.edf')))[bg_idx1-1: bg_idx2]
    sample_ehf = sorted(glob.glob(os.path.join(sample_folder, '*_raw.ehf')))[s_idx1-1: s_idx2]
    bg_ehf = sorted(glob.glob(os.path.join(bg_folder, '*_raw.ehf')))[s_idx1-1: s_idx2]
    
    files = zip(sample_files, bg_files, sample_ehf, bg_ehf)
    
    for f in files:
        img_samp = fabio.open(f[0]).data
        img_bg = fabio.open(f[1]).data
        n_samp, dark_samp = info(f[2], norm_method)
        n_bg, dark_bg = info(f[3], norm_method)
        subs = (img_samp)/n_samp - (img_bg)/n_bg
        #b, a = ai.separate(subs, percentile=85, npt_rad=1500, npt_azim=521, unit='q_nm^-1', method='splitpixel', mask=mask)
        w = ai.integrate1d(subs, npt=1301, mask=mask, unit='q_nm^-1', azimuth_range=(45,135), radial_range=(0.05, 0.5), method='splitpixel')
        path, name = os.path.split(f[0])
        if not os.path.exists(dat_folder):
            os.mkdir(dat_folder)
        file_name = os.path.join(dat_folder, name[:-3] + 'dat')
        ss_name = os.path.join(dat_folder, name[:-3] + 'tif')
        #if os.path.exists(file_name):
           # continue
        writer.set_filename(file_name)
         #sums.append(w[1].sum()/w[1][(w.radial>=0.21) & (w.radial<=0.22)].sum())   
        writer.save1D(file_name, dim1=w[0],I=w[1],dim1_unit='q_nm^-1')
        scipy.misc.toimage(subs, cmin=0.0, cmax=subs.max(),mode='F').save(ss_name)
        
def info(ehf, norm_method='trans'):
    with open(ehf) as t:
        tt = t.readlines()
        n1, n0 = tt[203:205]
        dark_path = os.path.split(ehf)[0]
        dark_file = tt[155].split(' ')[3][:-3] + 'edf'
        dark_img = os.path.join(dark_path, dark_file)
        n1 = float(n1.split(' ')[3])
        n0 = float(n0.split(' ')[3])
        if norm_method == 'trans':
            n = n1/n0
        elif norm_method == 'i0':
            n = n0
        else:
            n = 1.0
    return n, fabio.open(dark_img).data


