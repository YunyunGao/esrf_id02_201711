#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 00:52:23 2017

@author: Yunyun
"""


from integration import integration
from plot import substraction, plot_vet, plot_hor, plot_all_diff
import numpy as np

_raw_folder= ''
_raw_start = 0
_raw_end = 0
_saveto_folder = ''
_sample_folder = ''
_bg_folder = ''
_grid_ver = 1
_grid_hor =1
_ver_num = 1
_hor_num = 1
_diff_figname = ''

q_range = np.array([0.0, 0.7])

integration(_raw_folder, _saveto_folder, _raw_start, _raw_end)
data_matrix = substraction(_sample_folder, _bg_folder)
plot_hor(data_matrix, q_range, _grid_ver, _grid_hor, _hor_num-1)
plot_vet(data_matrix, q_range, _grid_ver, _grid_hor, _ver_num-1)
plot_all_diff(data_matrix, q_range, _diff_figname, _grid_ver, _grid_hor, q_range)
