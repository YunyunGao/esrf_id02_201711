#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  5 14:26:51 2017

@author: Yunyun
"""

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import numpy as np
import glob
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation 
import copy
import os


dev2_x_rel=np.array([ 46.5  ,  45.   ,  40.   ,  35.   ,  30.   ,  25.   ,  20.   ,
        15.   ,  10.   ,   9.   ,   8.   ,   7.   ,   6.   ,   5.   ,
         4.   ,   3.   ,   2.   ,   1.   ,   0.5  ,  -0.077,  -0.152,
        -0.227,  -0.5  ])

dev2_x_3 = np.array([45.9, 44.4, 39.4, 34.4, 29.4, 24.4, 19.4, 14.4, 9.4, 8.4,
                     7.4, 6.4, 5.4, 4.4, 3.4, 2.4,1.4,0.4,0.1,-0.677,-1.15])

def substraction(sample_folder, bg_folder):
    sample_files = sorted(glob.glob(os.path.join(sample_folder,'*.dat')))
    bg_files = sorted(glob.glob(os.path.join(bg_folder,'*.dat')))
    if len(sample_files) != len(bg_files):
        raise Exception("sample and background are not mathch!")
    sample = [pd.read_table(f, sep='\s+',skiprows=23, header=None).as_matrix() for f in sample_files]
    bg = [pd.read_table(f, sep='\s+',skiprows=23, header=None).as_matrix() for f in bg_files]
    sample = np.array(sample, dtype=np.float64)
    bg = np.array(bg, dtype=np.float64)
    subs_matrix = sample[:,:,1] - bg[:,:,1]/1.1
    q_range = sample[0,:,0]
    return subs_matrix, q_range

def plot_vet(subs_matrix, q_range, tot_vet, tot_hor, vet_num):
    fig, ax = plt.subplots(1, 1, figsize=[10,8],subplot_kw={'projection':'3d'})
    subs_matrix = subs_matrix.reshape(tot_vet,tot_hor,1301)
    #cNorm = colors.Normalize(vmin=0, vmax=14)
    #scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
    q_wf = np.inner(np.ones(subs_matrix.shape[0])[:,None],np.log10(q_range)[:,None])
    i_wf = np.log10(subs_matrix[:,vet_num,:]) 
    v_frame = np.linspace(0,tot_vet,tot_vet)
    ax.set_xlim([-2.2,-0.2])
    #ax.set_zlim([-2,8])
    ax.set_yticks(np.arange(0,tot_vet+1))
    ax.set_title(vet_num)
    ax.plot_wireframe(q_wf, v_frame[:,None], i_wf, cstride=0, colors=cm.jet((v_frame-v_frame.min())/(v_frame.max()-v_frame.min())))

def plot_hor(subs_matrix, q_range, tot_vet, tot_hor, hor_num):
    fig, ax = plt.subplots(1, 1, figsize=[10,8],subplot_kw={'projection':'3d'})
    subs_matrix = subs_matrix.reshape(tot_vet,tot_hor,1301)
    cNorm = colors.Normalize(vmin=0, vmax=14)
    scalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('jet'))
    x_rel = dev2_x_rel
    #for ind in range(subs_matrix.sh6ape[0]):
    q_wf = np.inner(np.ones(subs_matrix.shape[1])[:,None],np.log10(q_range)[:,None])
    i_wf = subs_matrix[hor_num,:,:]
    #ax.set_xlim([-2.2,-0.2])
    #ax.set_zlim([-20,100])
    ax.set_title(hor_num)
    ax.plot_wireframe(q_wf, x_rel[:,None], i_wf, cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))

def plot_all_diff(subs_matrix, q_range, fig_name, tot_vet, tot_hor):
    fig, ax_list = plt.subplots(4,4, figsize=[24,24], subplot_kw={'projection':'3d'})
    fig.suptitle(fig_name)
    subs_matrix = subs_matrix.reshape(tot_vet,tot_hor,1301)
    #ax.view_init(30,45)
    #ax.set_zlim([-20,100])
    q_clip = (q_range>=10**-2.3) & (q_range<=10**-0.6)
    i_wf = subs_matrix[:,:,q_clip] - subs_matrix[:,22,q_clip][:,None]
    q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range[q_clip][:,None]))
    x_rel = dev2_x_rel
    fig.subplots_adjust(hspace=0)
    for idx in range(0,tot_vet):
        ax_list[idx/4, idx%4].view_init(80,-65)
        ax_list[idx/4, idx%4].plot_wireframe(q_wf, x_rel[:,None], i_wf[idx], cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))
        ax_list[idx/4, idx%4].set_title(idx)
        ax_list[idx/4, idx%4].set_zlim([-20,100])

"""
#%%dev2b 300 515 - dev2b bg water
dev2_300_515, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2_300_515', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s')
plot_vet(dev2_300_515, q_range, 16, 23, 4)
plot_hor(dev2_300_515, q_range, 16, 23, 7)
#%%dev2d 300 515 - dev2b bg water
dev2d_300_515, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_515', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s')
plot_vet(dev2d_300_515, q_range, 16, 23, 7)
plot_hor(dev2d_300_515, q_range, 16, 23, 9)
#%%dev2d 300 111 - dev2b bg water
dev2d_300_111, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_111', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s')
#plot_vet(dev2d_300_111, q_range, 16, 23, 15)
#plot_hor(dev2d_300_111, q_range, 16, 23, 9)
#%%dev2c 150 111 - dev2b bg water
dev2c_150_111, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_111', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i_wbg_1s')
plot_vet(dev2c_150_111, q_range, 16, 23, 8)
plot_hor(dev2c_150_111, q_range, 16, 23, 7)
#%%dev2i bg water - dev2b bg water
water_comp, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2i_wbg_1s', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s')
#%%dev2s2d 300 515 - dev2b bg water without initiator
dev2s2d_300_515, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2d_300_515_woi', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s')
#%%dev2h2 300 515 - dev2i3
dev2h2_300_515, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2h2_300_515', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i3_wbg_1s')
#%%dev2f 150 111-dev2i3
dev2f_150_111, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2f_150_111', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i3_wbg_1s')
#%%dev2s2c 300 111 - dev2s2b
dev2s2c_300_111, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2c_300_111', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2b_wbg_1s')
plot_vet(dev2c_150_111, q_range, 16, 23, 8)
plot_hor(dev2c_150_111, q_range, 16, 23, 7)
plot_all_diff(dev2s2c_300_111, 'dev2s2c_300_111', 16, 23)
#%%dev2s4a 300 515 - dev2s3k
dev2s4a_300_515, q_range = substraction('/mntdirect/_data_visitor/sc4623/id02/proc/dev2s4a_300_515', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s3k_wbg_1s')
plot_vet(dev2s4a_300_515, q_range, 8, 21, 15)

#%%
#subs_matrix = dev2s2c_300_111
subs_matrix = dev2d_300_111

#%% animation single
fig, ax = plt.subplots(1,1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(16,23,1301)
ax.view_init(45,45)
ax.set_zlim([-6,4])
ax.set_xlim([-2.2,-0.2])
i_wf = np.log10(subs_matrix.transpose((1,0,2)))[:,1:15,:]
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range)[:,None])
v_frame = np.linspace(0,14,14)
wframe = None
while True:
    for idx in range(0,23):
        if wframe:
            ax.collections.remove(wframe)
        wframe = ax.plot_wireframe(q_wf, v_frame[:,None], i_wf[22-idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        ax.set_title(idx)
        plt.pause(0.1)
        
#%%animation single
fig, ax = plt.subplots(1,1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(8,21,1301)
ax.view_init(45,45)
ax.set_zlim([-6,4])
ax.set_xlim([-2.2,-0.2])
i_wf = np.log10(subs_matrix.transpose((1,0,2)))
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range)[:,None])
v_frame = np.linspace(0,8,8)
wframe = None
while True:
    for idx in range(15, 2, -1):
        if wframe:
            ax.collections.remove(wframe)
        wframe = ax.plot_wireframe(q_wf, v_frame[:,None], i_wf[20-idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        ax.set_title(idx)
        plt.pause(0.5)
#%%animation single
fig, ax = plt.subplots(1,1, figsize=[10,8],subplot_kw={'projection':'3d'})
subs_matrix = subs_matrix.reshape(16,23,1301)
ax.view_init(45,-45)
ax.set_zlim([-20,100])
#ax.set_xlim([-2.2,-0.2])
i_wf = subs_matrix
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range)[:,None])
x_rel = dev2_x_rel
wframe = None
while True:
    for idx in range(0,16):
        if wframe:
            ax.collections.remove(wframe)
        wframe = ax.plot_wireframe(q_wf, x_rel[:,None], i_wf[idx], cstride=0, colors=cm.jet(np.abs((v_frame-v_frame.mean()))/(v_frame.max()-v_frame.min())*2))
        ax.set_title(idx)
        plt.pause(0.5)
#%%
fig, ax_list = plt.subplots(4,4, figsize=[24,24], subplot_kw={'projection':'3d'})
fig.suptitle('dev2d_300_111')
subs_matrix = subs_matrix.reshape(16,23,1301)
ax.view_init(30,45)
ax.set_zlim([-20,100])
q_clip = (q_range>=10**-2.3) & (q_range<=10**-0.6)
i_wf = subs_matrix[:,:,q_clip] - subs_matrix[:,22,q_clip][:,None]
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range[q_clip][:,None]))
x_rel = dev2_x_rel
fig.subplots_adjust(hspace=0)
for idx in range(0,16):
    ax_list[idx/4, idx%4].view_init(80,-65)
    ax_list[idx/4, idx%4].plot_wireframe(q_wf, x_rel[:,None], i_wf[idx], cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))
    ax_list[idx/4, idx%4].set_title(idx)
    ax_list[idx/4, idx%4].set_zlim([-20,100])
#%%
fig, ax_list = plt.subplots(2,4, figsize=[18,10], subplot_kw={'projection':'3d'})
fig.suptitle('dev2s2c_300_111')
subs_matrix = subs_matrix.reshape(8,21,1301)
ax.view_init(30,45)
ax.set_zlim([-20,100])
i_wf = subs_matrix - subs_matrix[:,20,:][:,None,:]
q_wf = np.inner(np.ones(i_wf.shape[1])[:,None],np.log10(q_range)[:,None])
x_rel = dev2_x_3
fig.subplots_adjust(hspace=0)
for idx in range(0,8):
    ax_list[idx/4, idx%4].plot_wireframe(q_wf, x_rel[:,None], i_wf[idx], cstride=0, colors=cm.jet((x_rel-x_rel.min())/(x_rel.max()-x_rel.min())))
    ax_list[idx/4, idx%4].set_title(idx)
    ax_list[idx/4, idx%4].set_zlim([-20,100])

"""
    
    