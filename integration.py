#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  5 14:26:51 2017

@author: Yunyun
"""

import pyFAI
import fabio
import numpy as np
import glob
import pandas as pd
import os

#Initiation
detector = pyFAI.detectors.Detector(pixel1=88.3464e-6, pixel2=88.3464e-6, max_shape=(1920, 1920))
ai = pyFAI.AzimuthalIntegrator(dist=9.98818, poni1=988.2*88.3464e-6, 
                               poni2=906.9*88.3464e-6, 
                               detector=detector, wavelength=0.95057e-10)
mask = fabio.open("/mntdirect/_data_visitor/sc4623/id02/mask_10m.edf").data
mask[np.where(mask == 246)] = 1
writer = pyFAI.io.DefaultAiWriter(filename=None, engine=ai)

def integration(raw_folder, dat_folder, start_idx, end_idx):
    raw_files = sorted(glob.glob(raw_folder))[start_idx-1: end_idx]

    for f in raw_files:
        img = fabio.open(f)
        dark = fabio.open(os.path.join(img.header["DarkFilePath"], img.header["DarkFileName"][:-3] + 'edf'))
        b, a = ai.separate(img.data, percentile=50, npt_rad=1500, npt_azim=521, unit='q_nm^-1', method='splitpixel', mask=mask)
        n = ai.integrate1d(a, npt=100, mask=mask, unit='q_nm^-1', azimuth_range=(0,360), radial_range=(0.21, 0.22), dark=dark.data, method='splitpixel')
        w = ai.integrate1d(a, npt=1301, mask=mask, unit='q_nm^-1', azimuth_range=(0,360), radial_range=(0, 0.5), dark=dark.data, method='splitpixel', normalization_factor=n[1].sum())
        #nrm_i = w.intensity/w.intensity[(w.radial>=0.21) & (w.radial<=0.22)].sum()
        path, name = os.path.split(f)
        if not os.path.exists(dat_folder):
            os.mkdir(dat_folder)
        file_name = os.path.join(dat_folder, name[:-3] + 'dat')
        if os.path.exists(file_name):
            continue
        writer.set_filename(file_name)
         #sums.append(w[1].sum()/w[1][(w.radial>=0.21) & (w.radial<=0.22)].sum())   
        writer.save1D(file_name, dim1=w[0],I=w[1],dim1_unit='q_nm^-1')

"""
#%%dev2b bg water 369-736
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_wbg_1s',369, 736)
#%%dev2b 300 5:1:5 737-1104
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2b/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2_300_515',737, 1104)
#%%dev2d 300 5:1:5 1-368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_515',1, 368)
#%%dev2i=grid of dev2c  bg water 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2i/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i_wbg_1s',1, 368)
#%%dev2d 300 1:1:1 1105 1472
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_111',1105, 1472)
#%%dev2c300 1:1:1 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2c_150_111',1, 368)
#%%dev2s2d 300 5:1:5 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2d_300_515_woi',1, 368)
#%%dev2c 150 1:1:1 1 369
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2c_150_111',1, 368)
#%%dev2c 150 2.5:1:2.5 369 736
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2c_150_25125',369, 736)
#%%dev2c 150 5:1:5 737 1104
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2c/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2c_150_25125_re',737, 1104)
#%%dev2d 300 2.5:1:2.5 379 737
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_25125',379, 737)
#%%dev2d 300 2.5:1:2.5 737 1104
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2d_300_25125_re',737, 1104)
#%%dev2e 1500 5:1:5 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2e/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2e_1500_515',1, 368)
#%%dev2h2 300 5:1:5 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2h2/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2h2_300_515',1, 368)
#%%dev2hf 150 1:1:1 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2f/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2f_150_111',1, 368)
#%%dev2s2b water bg 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2b/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2b_wbg_1s',1, 368)
#%%dev2i3 water bg 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2i3/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i3_wbg_1s',1, 368)
#%%dev2i2 water bg 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2i2/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2i2_wbg_1s',1, 368)
#%%dev2s2b water bg 1 168
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2b/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2b_wbg_1s',1, 368)
#%%dev2s2c dev2s2d dev2s2e dev2s2f
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2c/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2c_300_111',1, 368)
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2d/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2d_300_515',1, 368)
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2e/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2e_150_515',1, 368)
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s2f/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s2f_150_111',1, 368)
#%%dev2s3a bg water 1 368
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s3a/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s3a_wbg_1s',1, 368)
#%%dev2s3k bg water 1 168
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s3k/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s3k_wbg_1s',1, 168)
#%%dev2s4a 300 515
integration('/mntdirect/_data_visitor/sc4623/id02/real_data/dev2s4a/*_raw.edf', '/mntdirect/_data_visitor/sc4623/id02/proc/dev2s4a_300_515',213, 381)


#%%dev1 water 1s
file_folder = "/mntdirect/_data_visitor/sc4623/id02/real_data/dev1/*_01*_raw.edf"
#dark_files = sorted(glob.glob("/mntdirect/_data_visitor/sc4623/id02/real_data/dev1/*_01*_dark.edf"))[23:47]
raw_files = sorted(glob.glob(file_folder))[375: 759]

for f in raw_files:
    img = fabio.open(f)
    dark = fabio.open(os.path.join(img.header["DarkFilePath"], img.header["DarkFileName"][:-3] + 'edf'))
    b, a = ai.separate(img.data, percentile=50, npt_rad=1500, npt_azim=521, unit='q_nm^-1', method='splitpixel', mask=mask)
    n = ai.integrate1d(a, npt=100, mask=mask, unit='q_nm^-1', azimuth_range=(0,360), radial_range=(0.21, 0.22), dark=dark.data, method='splitpixel')
    w = ai.integrate1d(a, npt=1301, mask=mask, unit='q_nm^-1', azimuth_range=(0,360), radial_range=(0, 0.5), dark=dark.data, method='splitpixel', normalization_factor=n[1].sum())
    #nrm_i = w.intensity/w.intensity[(w.radial>=0.21) & (w.radial<=0.22)].sum()
    path, name = os.path.split(f)
    if not os.path.exists('/mntdirect/_data_visitor/sc4623/id02/proc/dev1_wbg_1s'):
        os.mkdir('/mntdirect/_data_visitor/sc4623/id02/proc/dev1_wbg_1s')
    writer.set_filename('/mntdirect/_data_visitor/sc4623/id02/proc/dev1_wbg_1s/'+ name[:-3] + 'dat')
     #sums.append(w[1].sum()/w[1][(w.radial>=0.21) & (w.radial<=0.22)].sum())   
    writer.save1D('/mntdirect/_data_visitor/sc4623/id02/proc/dev1_wbg_1s/'+ name[:-3] + 'dat',dim1=w[0],I=w[1],dim1_unit='q_nm^-1')
    
#%%
"""